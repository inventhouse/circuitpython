# Simple random blinker
# Copyright (c) 2019 Benjamin Holt -- MIT License

import board
from digitalio import DigitalInOut, Direction, Pull
import random
import time

led = DigitalInOut(board.D13)
led.direction = Direction.OUTPUT
led.value = True

while True:
    led.value = not led.value
    time.sleep(random.randint(1, 10) / 10)  # delay between .1 and 1 seconds
###
