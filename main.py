# CircuitPython "app switcher"
# Copyright (c) 2018-2019 Benjamin Holt -- MIT License

print("[[  Main Start  ]]")
app = None  # Import as app to have main() called

# Just import the script you want to run and comment out others to switch
import randblink
# import runloopStats as app

if "main" in dir(app):
    app.main()

print("[[  Main Done  ]]")
###
