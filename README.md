# CircuitPython

A collection of small utilities for [CircuitPython](https://circuitpython.org)

- **`cpycopy` - Copy files to your board without macOS's `._` shadow-files**
    > macOS creates hidden extra files beginning with `._` on systems that don't support its resources and extended attributes, bloating each file it copies to the device by _~4KB_ whether it needs it or not!  (Use `ls -al` to see them.)  This is a small script to use on the "host" to copy files or directories without that pesky `._` junk; `cpycopy -h` will print help.

- **`main.py` - A simple "app switcher" for CircuitPython**
    > I _**very**_ quickly tired of changing my program name every time I copied it to the board to run, so I made a simple "universal" `main.py` that lets me use descriptive program names and easily switch between them.
    >
    > This "runs" programs by importing them; to create libraries that can be imported into other programs but have a built-in demo or tests, define a `main()` function that runs the demo or tests and import it as `app` to have that called.

- **`randblink.py` - A small LED blinker**
    > Just a little program to be an example for using `main.py` above.

---
